package com.example.gameadvisor.theme;

import com.example.gameadvisor.MessageController;
import com.example.gameadvisor.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class ThemeController extends MessageController {

    @ModelAttribute("module")
    String module() {
        return "themes";
    }
    
    private final ThemeService themeService;

    @Autowired
    public ThemeController(ThemeService themeService) {
        this.themeService = themeService;
    }

    @GetMapping("/themes")
    public String main(Model model) {
        model.addAttribute("items", this.themeService.findAll());
        addMessageToModel(model);
        return "items";
    }

    @PostMapping("/themes")
    public String add(@RequestParam(required = false) String nom) {
        if (!nom.isEmpty()) {
            themeService.add(new Theme(nom));
            setSuccessMessage("Le thème \"" + nom + "\" à bien été ajouté.");
        }
        return "redirect:/themes";
    }

    @PostMapping("/themes/{id}")
    public String update(@PathVariable Long id, @RequestParam() String nom) {
        Optional<Theme> theme = themeService.getById(id);
        if (theme.isPresent()) {
            themeService.update(theme.get(), nom);
            setSuccessMessage("Le thème à bien été renommé en \"" + nom + "\".");
        } else {
            setErrorMessage("Le thème n'a pas pu être modifié.");
        }
        return "redirect:/themes";
    }

    @GetMapping("/themes/{id}/supprimer")
    public String delete(@PathVariable Long id) {
        themeService.deleteById(id);
        setSuccessMessage("Le thème à bien été supprimé.");
        return "redirect:/themes";
    }
}
