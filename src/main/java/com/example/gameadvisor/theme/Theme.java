package com.example.gameadvisor.theme;

import org.apache.commons.text.WordUtils;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.persistence.*;
import java.util.Objects;

@ApplicationScope
@Entity
@Table(name="theme_jeu")
public class Theme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom_theme")
    private String nom;

    public Theme() {
    }

    public Theme(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return WordUtils.capitalize(nom, '-', ' ');
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Theme{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theme theme = (Theme) o;
        return Objects.equals(id, theme.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
