package com.example.gameadvisor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameadvisorApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameadvisorApplication.class, args);
    }

}
