package com.example.gameadvisor.jeu;

import com.example.gameadvisor.editeur.Editeur;
import com.example.gameadvisor.genre.Genre;
import com.example.gameadvisor.note.Note;
import com.example.gameadvisor.theme.Theme;
import com.example.gameadvisor.type.Type;
import org.apache.commons.text.WordUtils;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@ApplicationScope
@Entity
@Table(name="jeu")
public class Jeu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nom_jeu")
    private String nom;

    @ManyToOne( targetEntity = Type.class, fetch = FetchType.LAZY )
    @NotFound( action = NotFoundAction.IGNORE )
    @JoinColumn(name = "id_type", referencedColumnName = "id", nullable = true)
    private Type type;

    @ManyToOne( targetEntity = Genre.class, fetch = FetchType.LAZY )
    @NotFound( action = NotFoundAction.IGNORE )
    @JoinColumn(name="id_genre", referencedColumnName = "id", nullable = true)
    private Genre genre;

    @ManyToOne( targetEntity = Theme.class, fetch = FetchType.LAZY )
    @NotFound( action = NotFoundAction.IGNORE )
    @JoinColumn(name = "id_theme", referencedColumnName = "id", nullable = true)
    private Theme theme;

    @ManyToOne( targetEntity = Editeur.class, fetch = FetchType.LAZY )
    @NotFound( action = NotFoundAction.IGNORE )
    @JoinColumn(name="id_editeur", referencedColumnName = "id", nullable = false)
    private Editeur editeur;

    @Column(name = "age_minimum")
    private int ageMinimum;

    @Column(name="nombre_joueurs_minimum")
    private int nbJoueurMini;

    @Column(name="nombre_joueurs_maximum")
    private int nbJoueurMaxi;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "jeu", cascade = CascadeType.ALL)
    private Collection<Note> notes;

    public Jeu() {
    }

    public Jeu(String nom, Type type, Genre genre, Theme theme, Editeur editeur, int ageMinimum, int nbJoueurMini, int nbJoueurMaxi) {
        this.nom = nom;
        this.type = type;
        this.genre = genre;
        this.theme = theme;
        this.editeur = editeur;
        this.ageMinimum = ageMinimum;
        this.nbJoueurMini = nbJoueurMini;
        this.nbJoueurMaxi = nbJoueurMaxi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return WordUtils.capitalizeFully(nom);
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Collection<Note> getNotes() {
        return notes;
    }

    public void setNotes(Collection<Note> notes) {
        this.notes = notes;
    }

    public Theme getTheme() { return theme; }

    public void setTheme(Theme theme) { this.theme = theme; }

    public Genre getGenre() { return genre; }

    public void setGenre(Genre genre) { this.genre = genre; }

    public Editeur getEditeur() { return editeur; }

    public void setEditeur(Editeur editeur) {
        this.editeur = editeur;
    }

    public int getAgeMinimum() {
        return ageMinimum;
    }

    public void setAgeMinimum(int ageMinimum) {
        this.ageMinimum = ageMinimum;
    }

    public int getNbJoueurMini() {
        return nbJoueurMini;
    }

    public void setNbJoueurMini(int nbJoueurMini) {
        this.nbJoueurMini = nbJoueurMini;
    }

    public int getNbJoueurMaxi() {
        return nbJoueurMaxi;
    }

    public void setNbJoueurMaxi(int nbJoueurMaxi) {
        this.nbJoueurMaxi = nbJoueurMaxi;
    }

    @Override
    public String toString() {
        return "Jeu{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", type=" + type +
                ", genre=" + genre +
                ", theme=" + theme +
                ", editeur=" + editeur +
                ", ageMinimum=" + ageMinimum +
                ", nbJoueurMini=" + nbJoueurMini +
                ", nbJoueurMaxi=" + nbJoueurMaxi +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jeu jeu = (Jeu) o;
        return id.equals(jeu.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
