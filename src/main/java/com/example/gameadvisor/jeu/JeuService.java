package com.example.gameadvisor.jeu;

import com.example.gameadvisor.editeur.Editeur;
import com.example.gameadvisor.genre.Genre;
import com.example.gameadvisor.note.Note;
import com.example.gameadvisor.theme.Theme;
import com.example.gameadvisor.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class JeuService {
    private final JeuRepository jeuRepository;
    private final JeuDao jeuDao;

    @Autowired
    public JeuService(JeuRepository jeuRepository, JeuDao jeuDao) {
        this.jeuRepository = jeuRepository;
        this.jeuDao = jeuDao;
    }

    public Collection<Jeu> findAll() {
        return jeuRepository.findAll();
    }

    public Optional<Jeu> getById(Long id) {
        return jeuRepository.findById(id);
    }

    public Jeu add(Jeu jeu)
    {
        return jeuRepository.save(jeu);
    }

    public Jeu update(Jeu jeu, String nom, Type type, Genre genre, Theme theme, Editeur editeur, int ageMinimum, int nbJoueursMin, int nbJoueursMax)
    {
        jeu.setNom(nom);
        jeu.setAgeMinimum(ageMinimum);
        jeu.setNbJoueurMini(nbJoueursMin);
        jeu.setNbJoueurMaxi(nbJoueursMax);
        jeu.setType(type);
        jeu.setGenre(genre);
        jeu.setTheme(theme);
        jeu.setEditeur(editeur);
        return jeuRepository.save(jeu);
    }

    public Jeu insert(Long id, Jeu jeu)
    {
        jeu.setId(id);
        return jeuRepository.save(jeu);
    }

    public void deleteById(Long id)
    {
        jeuRepository.deleteById(id);
    }

    public Optional<Collection<Jeu>> filter(Type type, Genre genre, Theme theme, Editeur editeur, int ageMinimum, int nbJoueurs)
    {
        return jeuRepository.findAllByTypeAndGenreAndThemeAndEditeurAndAgeMinimumLessThanEqualAndNbJoueurMiniLessThanEqualAndNbJoueurMaxiGreaterThanEqual(type, genre, theme, editeur, ageMinimum, nbJoueurs, nbJoueurs);
    }

    public Optional<Collection<Jeu>> getJeuxByType(Type type){
        return jeuRepository.findByType(type);
    }

    public Collection<Jeu> findJeuxByMultipleFilters(Type type, Genre genre, Theme theme, Editeur editeur, int nbJoueurs, int ageMini) {
        return sortJeuxByNote(jeuDao.findJeuxByMultipleFilters(type, genre, theme, editeur, nbJoueurs, ageMini));
    }


    public static Collection<Jeu> sortJeuxByNote(Collection<Jeu> jeux)
    {
       SortedMap<Double, List<Jeu>> mapJeuxNote = new TreeMap<>((o1, o2) -> -o1.compareTo(o2));
        for (Jeu jeu : jeux) {
            double moyenne = moyenne(jeu);
            if(!mapJeuxNote.containsKey(moyenne))
                mapJeuxNote.put(moyenne(jeu), new ArrayList<>());
            mapJeuxNote.get(moyenne).add(jeu);
        }

        Set<Map.Entry<Double, List<Jeu>>> s = mapJeuxNote.entrySet();
        Iterator<Map.Entry<Double, List<Jeu>>> i = s.iterator();
        List<Jeu> finalList = new ArrayList<>();

        while (i.hasNext())
        {
            Map.Entry<Double, List<Jeu>> m = i.next();
            List<Jeu> list = m.getValue();
            finalList.addAll(list);
        }
        return finalList;
    }

    public static double calculNote(Jeu jeu) {
        Collection<Note> notes = jeu.getNotes();
        return calculNote(jeu, notes.toArray(new Note[0]));
    }

    public static double calculNote(Jeu jeu, Note... testeurs) {
        Object[] tab = new Object[testeurs.length * 2];
        for (int i = 0, j = 0;i < testeurs.length ; i++,j+=2) {
            tab[j] = testeurs[i].getTesteur();
            tab[j+1] = testeurs[i].getNote();
        }
        return calculNote(jeu.getNom(), jeu.getType().getNom(), jeu.getTheme().getNom(), jeu.getGenre().getNom(), jeu.getEditeur().getNom(), 0,0, tab);
    }

    public static double moyenne(Jeu jeu)
    {
        Collection<Note> notes = jeu.getNotes();
        double moyenne = 0;
        for(Note note: notes)
            moyenne += note.getNote();
        return moyenne == 0.0 ? moyenne : moyenne/notes.size();
    }

    private static double calculNote(String nomJeu,String typeJeu,String themeJeu,String genreJeu,String editeurJeu,int premierZero,int deuxiemeZero, Object... testeurs)
    {
        int temp = 1;
        --deuxiemeZero;
        if(testeurs.length == 2)
        {
            premierZero += (Integer)(int) testeurs[1];
            if (themeJeu == "science-fiction" && (String)testeurs[0] == "Didier Loyal" && (Integer)testeurs[1]<=9) {
                ++premierZero;
                temp-=2;
            }
            if (genreJeu == "gestion" && (String)testeurs[0] == "Armande Moly" && (Integer)testeurs[1]<10) {
                premierZero++;
            }
            if (typeJeu == "jeu de cartes" && (String) testeurs[0] == "Gaston Portaleau" && (Integer)testeurs[1]>0){
                premierZero -=1;
            }
            if (editeurJeu == "édijeu" && (String)testeurs[0] == "Liz Smallhead" && (Integer)testeurs[1]>=2){
                premierZero-=2;
                temp+=4;
            }
            if (themeJeu == "contemporain" && (String)testeurs[0] == "Stefan Bergdorf" && (Integer)testeurs[1] >= 3 && (Integer)testeurs[1] <= 7){
                premierZero=premierZero+1/2;
            }
            deuxiemeZero += 2;
            return (double) premierZero / (Double) (double) deuxiemeZero;
        }
        if (testeurs.length == 4) {
            if (temp > 1) {
                premierZero += (Integer) testeurs[1] + (Integer)
                        (int) testeurs[3];
                deuxiemeZero += 3;
                if (themeJeu == "science-fiction" && ((String) testeurs[0] == "Didier Loyal" ||
                        (String) testeurs[2] == "Didier Loyal") && ((Integer) testeurs[1] <= 9 || (Integer) testeurs[3] <= 9)) {
                    ++premierZero;
                    temp -= 2;
                }
                if (genreJeu == "gestion" && ((String) testeurs[0] == "Armande Moly" || (String) testeurs[2] == "Armande Moly ") && ((Integer) testeurs[1] < 10 || (Integer) testeurs[3] < 10)) {
                    premierZero++;
                }
                if (typeJeu == " jeu de cartes " && ((String) testeurs[0] == " Gaston Portaleau " || (String) testeurs[2] == " Gaston Portaleau ") && ((Integer) testeurs[1] > 0 || (Integer) testeurs[3] > 0)) {
                    premierZero -= 1;
                }
                if (editeurJeu == "édijeu" && ((String) testeurs[0] == "Liz Smallhead" || (String) testeurs[2] == "Liz Smallhead ") && ((Integer) testeurs[1] >= 2 || (Integer) testeurs[3] >= 2)) {
                    premierZero -= 2; temp += 4;
                }
                if (themeJeu == "contemporain" && ((String) testeurs[0] == "Stefan Bergdorf" || (String) testeurs[2] == "Stefan Bergdorf") && (((Integer) testeurs[1] >= 3 && (Integer) testeurs[1] <= 7) || ((Integer) testeurs[3] >= 3 && (Integer) testeurs[3] <= 7))) {
                    premierZero = premierZero + 1 / 2;
                }
                return (double) premierZero / (double) deuxiemeZero;
            }
        } if (testeurs.length > 3) {
        premierZero += (Integer) testeurs[1];
        deuxiemeZero += 2;
        Object[] newtesteurs = new Object[testeurs.length - 2];
        if (themeJeu == "science-fiction" && (String) testeurs[0] == "Didier Loyal" && (Integer) testeurs[1] <= 9) {
            ++premierZero;
            temp -= 2;
        }
        if (genreJeu == " gestion" && (String) testeurs[0] == " Armande Moly" && (Integer) testeurs[1] < 10) {
            premierZero++;
        }
        if (typeJeu == " jeu de cartes" && (String) testeurs[0] == " Gaston Portaleau" && (Integer) testeurs[1] > 0) {
            premierZero -= 1;
        }
        if (editeurJeu == " édijeu" && (String) testeurs[0] == " Liz Smallhead" && (Integer) testeurs[1] >= 2) {
            premierZero -= 2; temp += 4;
        }
        if (themeJeu == "contemporain" && (String) testeurs[0] == "Stefan Bergdorf" && (Integer) testeurs[1] >= 3 && (Integer) testeurs[1] <= 7) {
            premierZero = premierZero + 1 / 2;
        }
        System.arraycopy(testeurs, 2, newtesteurs, 0, testeurs.length - 2);
        return calculNote(nomJeu, typeJeu, themeJeu, genreJeu, editeurJeu, premierZero, deuxiemeZero, newtesteurs);
    } if (testeurs.length == 0) {
        return 0.0;
    }
        return -1.0;
    }
}
