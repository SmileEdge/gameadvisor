package com.example.gameadvisor.jeu;

import com.example.gameadvisor.editeur.Editeur;
import com.example.gameadvisor.genre.Genre;
import com.example.gameadvisor.theme.Theme;
import com.example.gameadvisor.type.Type;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;

public interface JeuRepository extends JpaRepository<Jeu, Long> {
    Optional<Collection<Jeu>> findByType(Type type);
    Optional<Collection<Jeu>> deleteAllByEditeur(Editeur editeur);
    Optional<Collection<Jeu>> findAllByTypeAndGenreAndThemeAndEditeurAndAgeMinimumLessThanEqualAndNbJoueurMiniLessThanEqualAndNbJoueurMaxiGreaterThanEqual(Type type, Genre genre, Theme theme, Editeur editeur, int ageMinimum, int nbJoueurMini, int nbJoueurMaxi);
}
