package com.example.gameadvisor.type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class TypeService {

    private final TypeRepository typeRepository;

    @Autowired
    public TypeService(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    public Collection<Type> findAll() {
        return typeRepository.findAll();
    }

    public Optional<Type> getById(Long id){
        return typeRepository.findById(id);
    }

    public Type add(Type type)
    {
        return typeRepository.save(type);
    }

    public Type update(Type type, String nom)
    {
        type.setNom(nom);
        return typeRepository.save(type);
    }

    public Type insert(Long id, Type type)
    {
        type.setId(id);
        return typeRepository.save(type);
    }

    public void deleteById(Long id)
    {
        typeRepository.deleteById(id);
    }
}
