package com.example.gameadvisor;

import org.springframework.ui.Model;

public abstract class MessageController {

    private String type;
    private String message;

    public MessageController() {
        this.message = "";
        this.type = "info";
    }

    protected void addMessageToModel(Model model) {
        if (!this.message.isEmpty()) {
            model.addAttribute("message", this.message);
            model.addAttribute("messageType", this.type);
            this.message = "";
            this.type = "info";
        }
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    protected void setSuccessMessage(String message) {
        this.setMessage(message);
        this.type = "success";
    }

    protected void setErrorMessage(String message) {
        this.setMessage(message);
        this.type = "error";
    }

    protected void setWarningMessage(String message) {
        this.setMessage(message);
        this.type = "warning";
    }
}
