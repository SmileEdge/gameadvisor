package com.example.gameadvisor.editeur;

import com.example.gameadvisor.jeu.JeuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
public class EditeurService {
    private final EditeurRepository editeurRepository;
    private final JeuRepository jeuRepository;

    @Autowired
    public EditeurService(EditeurRepository editeurRepository, JeuRepository jeuRepository) {
        this.editeurRepository = editeurRepository;
        this.jeuRepository = jeuRepository;
    }

    public Collection<Editeur> findAll() {
        return editeurRepository.findAll();
    }

    public Optional<Editeur> getById(Long id){
        return editeurRepository.findById(id);
    }

    public Editeur add(Editeur editeur)
    {
        return editeurRepository.save(editeur);
    }
    
    public Editeur update(Editeur editeur, String nom)
    {
        editeur.setNom(nom);
        return editeurRepository.save(editeur);
    }

    public Editeur insert(Long id, Editeur editeur)
    {
        editeur.setId(id);
        return editeurRepository.save(editeur);
    }

    @Transactional
    public void deleteById(Long id)
    {
        Optional<Editeur> editeur = editeurRepository.findById(id);
        if(editeur.isPresent())
        {
            jeuRepository.deleteAllByEditeur(editeur.get());
        }
        editeurRepository.deleteById(id);
    }
}
