package com.example.gameadvisor.editeur;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EditeurRepository extends JpaRepository<Editeur, Long> {
}
