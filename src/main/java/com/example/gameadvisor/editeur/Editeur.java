package com.example.gameadvisor.editeur;

import org.apache.commons.text.WordUtils;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.persistence.*;
import java.util.Objects;

@ApplicationScope
@Entity
@Table(name="editeur_jeu")
public class Editeur{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nom_editeur")
    private String nom;

    public Editeur() {
    }

    public Editeur(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return WordUtils.capitalize(nom);
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Editeur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Editeur editeur = (Editeur) o;
        return Objects.equals(id, editeur.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
