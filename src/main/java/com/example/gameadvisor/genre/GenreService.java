package com.example.gameadvisor.genre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class GenreService {
    private final GenreRepository genreRepository;

    @Autowired
    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public Collection<Genre> findAll() {
        return genreRepository.findAll();
    }

    public Optional<Genre> getById(Long id){
        return genreRepository.findById(id);
    }

    public Genre add(Genre genre)
    {
        return genreRepository.save(genre);
    }

    public Genre update(Genre genre, String nom)
    {
        genre.setNom(nom);
        return genreRepository.save(genre);
    }

    public Genre insert(Long id, Genre genre)
    {
        genre.setId(id);
        return genreRepository.save(genre);
    }

    public void deleteById(Long id)
    {
        genreRepository.deleteById(id);
    }
}
