package com.example.gameadvisor.genre;

import com.example.gameadvisor.MessageController;
import com.example.gameadvisor.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class GenreController extends MessageController {

    @ModelAttribute("module")
    String module() {
        return "genres";
    }

    private final GenreService genreService;

    @Autowired
    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping("/genres")
    public String main(Model model) {
        model.addAttribute("items", this.genreService.findAll());
        addMessageToModel(model);
        return "items";
    }

    @PostMapping("/genres")
    public String add(@RequestParam(required = false) String nom) {
        if (!nom.isEmpty()) {
            genreService.add(new Genre(nom));
            setSuccessMessage("Le genre \"" + nom + "\" à bien été ajouté.");
        }
        return "redirect:/genres";
    }

    @PostMapping("/genres/{id}")
    public String update(@PathVariable Long id, @RequestParam() String nom) {
        Optional<Genre> genre = genreService.getById(id);
        if (genre.isPresent()) {
            genreService.update(genre.get(), nom);
            setSuccessMessage("Le genre à bien été renommé en \"" + nom + "\".");
        } else {
            setErrorMessage("Le genre n'a pas pu être modifié.");
        }
        return "redirect:/genres";
    }

    @GetMapping("/genres/{id}/supprimer")
    public String delete(@PathVariable Long id) {
        genreService.deleteById(id);
        setSuccessMessage("Le genre à bien été supprimé.");
        return "redirect:/genres";
    }
}
