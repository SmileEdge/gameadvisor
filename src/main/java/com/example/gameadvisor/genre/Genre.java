package com.example.gameadvisor.genre;

import org.apache.commons.text.WordUtils;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.persistence.*;
import java.util.Objects;

@ApplicationScope
@Entity
@Table(name="genre_jeu")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nom_genre")
    private String nom;

    public Genre() {
    }

    public Genre(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return WordUtils.capitalize(nom);
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(id, genre.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
