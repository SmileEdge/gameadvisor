package com.example.gameadvisor.testJeu;

import com.example.gameadvisor.editeur.Editeur;
import com.example.gameadvisor.editeur.EditeurService;
import com.example.gameadvisor.genre.Genre;
import com.example.gameadvisor.genre.GenreService;
import com.example.gameadvisor.jeu.Jeu;
import com.example.gameadvisor.jeu.JeuService;
import com.example.gameadvisor.theme.Theme;
import com.example.gameadvisor.theme.ThemeService;
import com.example.gameadvisor.type.Type;
import com.example.gameadvisor.type.TypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JeuTest {

    @Autowired
    JeuService jeuService;

    @Autowired
    TypeService typeService;

    @Autowired
    GenreService genreService;

    @Autowired
    ThemeService themeService;

    @Autowired
    EditeurService editeurService;

    @Test
    public void GameListNotEmpty() {
        assertNotEquals(jeuService.findAll(), 0);
    }

    @Test public void print(){
        System.out.println(jeuService.findAll());
    }

    @Test
    public void getTypeOfGame() {
        Optional<Jeu> jeu = jeuService.getById(1L);
        assertTrue(jeu.isPresent());
        Type type = jeu.get().getType();
        assertEquals(type.getNom(), "Jeu de plateau");
    }

    @Test
    public void getGamesByType() {
        Optional<Type> type = typeService.getById(1L);
        assertTrue(type.isPresent());
        Optional<Collection<Jeu>> jeux = jeuService.getJeuxByType(type.get());
        assertTrue(jeux.isPresent());
        Iterator<Jeu> iterator = jeux.get().iterator();
        Jeu jeu = iterator.next();
        assertEquals(jeu.getType().getNom(), type.get().getNom());

    }

    @Test
    public void filter()
    {
        Optional<Type> type = typeService.getById(2L);
        Optional<Genre> genre = genreService.getById(1L);
        Optional<Theme> theme = themeService.getById(4L);
        Optional<Editeur> editeur = editeurService.getById(3L);
        assertTrue(type.isPresent());
        assertTrue(genre.isPresent());
        assertTrue(theme.isPresent());
        assertTrue(editeur.isPresent());

        Optional<Collection<Jeu>> jeux = jeuService.filter(type.get(), genre.get(), theme.get(), editeur.get(), 5, 5);
        assertTrue(jeux.isPresent());
        System.out.println(jeux);
    }

    @Test
    public void testFindJeuByMultipleFilters()
    {
        Type type = typeService.getById(2L).get();
        Genre genre = genreService.getById(1L).get();
        Theme theme = themeService.getById(4L).get();
        Editeur editeur = editeurService.getById(3L).get();
        Collection<Jeu> jeu = jeuService.findJeuxByMultipleFilters(type,genre,theme, editeur,3,15);
        System.out.println("");

        assertEquals(jeu.size(), 1);

    }

    @Test
    public void testFindJeuWithOnlyAge()
    {
        Collection<Jeu> jeux = jeuService.findJeuxByMultipleFilters(null, null,null, null, -1, 5);
        assertEquals(jeux.size(), 4);
    }

    @Test
    public void testSortJeuByNote()
    {
        Collection<Jeu> jeuxTotest = jeuService.findAll();
        Collection<Jeu> jeux = jeuService.sortJeuxByNote(jeuxTotest);
        assertTrue(jeux.size() > 0);
        Iterator<Jeu> itr = jeux.iterator();
        assertTrue(itr.hasNext());
        assertEquals(0.0, JeuService.moyenne(itr.next()), 0.0);
        Jeu last = null;
        while(itr.hasNext()) {
            last = itr.next();
        }
        assertEquals(9.5, JeuService.moyenne(last), 0.0);
    }

}

