package com.example.gameadvisor.testJeu;

import com.example.gameadvisor.jeu.Jeu;
import com.example.gameadvisor.jeu.JeuService;
import com.example.gameadvisor.note.Note;
import com.example.gameadvisor.note.NoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestCalculChelou {
    @Autowired
    JeuService jeuService;

    @Autowired
    NoteService noteService;

    @Test
    public void verifyIfSimplyWorksWithOneReviewer(){
        Optional<Jeu> jeu = jeuService.getById(1L);
        assertTrue(jeu.isPresent());

        Optional<Note> first = noteService.getById(1L);
        assertTrue(first.isPresent());

        double result = JeuService.calculNote(jeu.get(), first.get());
        System.out.println(result);
    }

    @Test
    public void verifyIfSimplyWorksWithMultipleReviewers(){
        Optional<Jeu> jeu = jeuService.getById(1L);
        assertTrue(jeu.isPresent());

        Optional<Note> first = noteService.getById(1L);
        assertTrue(first.isPresent());
        Optional<Note> second = noteService.getById(2L);
        assertTrue(second.isPresent());

        double result = JeuService.calculNote(jeu.get(), first.get(), second.get());
        System.out.println(result);
    }

    @Test
    public void listNoteJeux()
    {
        Collection<Jeu> listeJeux = jeuService.findAll();
        for (Jeu jeu : listeJeux) {
            assertEquals(JeuService.calculNote(jeu), JeuService.moyenne(jeu), 0.0);
        }
    }
}

