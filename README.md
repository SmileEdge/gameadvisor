[![pipeline status](https://gitlab.com/SmileEdge/gameadvisor/badges/master/pipeline.svg)](https://gitlab.com/SmileEdge/gameadvisor/-/commits/master)
# Visualisation de notes de jeux de société

## Pages web

Les pages web suivantes sont attendues dans le cadre de ce projet :

- Une page d'accueil permettant de choisir les filtres sur les jeux (type,
    genre, thème, nombre de joueurs, âge minimum conseillé, éditeur)
- Une page de visualisation des notes affichant l'ensemble des jeux de
    société correspondant aux filtres avec leurs notes, triés par notes
    décroissantes
- Une page permettant d'afficher les différents types de jeu de société, de
    les modifier, d'en ajouter et d'en supprimer. Supprimer un type ne
    supprime pas les jeux associés.
- Une page permettant d'afficher les différents genres de jeu de société, de
    les modifier, d'en ajouter et d'en supprimer. Supprimer un genre ne
    supprime pas les jeux associés.
- Une page permettant d'afficher les différents thèmes de jeu de société,
    de les modifier, d'en ajouter et d'en supprimer. Supprimer un thème ne
    supprime pas les jeux associés.
- Une page permettant d'afficher les différents éditeurs de jeu de société,
    de les modifier, d'en ajouter et d'en supprimer. Supprimer un éditeur
    supprime également tous les jeux associés.

## Navigation

La navigation entre les différentes pages web s'effectue de la manière
suivante :

- La page d'accueil est toujours accessible directement via un hyperlien
    présent sur toutes les pages
- La page de visualisation des notes est accessible uniquement depuis la
    page d'accueil, en validant les filtres sélectionnés
- Les pages d'édition des jeux, des types, des genres, des thèmes et des
    éditeurs sont accessibles depuis un menu situé sur la page d'accueil


- Valider une modification, un ajout ou une suppression d'un élément (jeu,
    type, genre, thème ou éditeur) recharge la même page, avec un
    message indiquant si l'opération s'est bien passée

## Calcul de la note

La note d'un jeu de société est calculée par la méthode suivante, fourni par
notre spécialiste Java Urist McDeveloper (paramètres d'appel : nom du jeu,
type du jeu, thème du jeu, genre du jeu, éditeur du jeu, 0, 0, nom du testeur1,
note du testeur1, nom du testeur2, note du testeur2... ) :

## Structure de la base de données

Le script SQL permettant de créer et initialiser la base de données est creation_base.sql.

## Évolutions futures

Si ce projet donne satisfaction, voici les évolutions envisagées dans le futur :

- Remplacement de la méthode de calcul de la note d'un jeu
- Prise en compte des modifications sur un élément (avec message
    indiquant le statut) sans recharger la page
- Sécuriser la navigation:aucune page (en dehors de la page d'accueil)
    n'est accessible via URL, tout doit passer par l'application